EESchema Schematic File Version 4
LIBS:atmega328-usb-cache
EELAYER 30 0
EELAYER END
$Descr A4 11693 8268
encoding utf-8
Sheet 1 1
Title ""
Date ""
Rev ""
Comp ""
Comment1 ""
Comment2 ""
Comment3 ""
Comment4 ""
$EndDescr
$Comp
L MCU_Microchip_ATmega:ATmega328-PU U1
U 1 1 5D888701
P 3580 4060
F 0 "U1" H 2936 4106 50  0000 R CNN
F 1 "ATmega328-PU" H 2936 4015 50  0000 R CNN
F 2 "Housings_DIP:DIP-28_W7.62mm" H 3580 4060 50  0001 C CIN
F 3 "http://ww1.microchip.com/downloads/en/DeviceDoc/ATmega328_P%20AVR%20MCU%20with%20picoPower%20Technology%20Data%20Sheet%2040001984A.pdf" H 3580 4060 50  0001 C CNN
	1    3580 4060
	1    0    0    -1  
$EndComp
$Comp
L Device:Crystal Y1
U 1 1 5D88AAAB
P 4700 3500
F 0 "Y1" H 4700 3768 50  0000 C CNN
F 1 "Crystal" H 4700 3677 50  0000 C CNN
F 2 "Crystals:Crystal_HC18-U_Vertical" H 4700 3500 50  0001 C CNN
F 3 "~" H 4700 3500 50  0001 C CNN
	1    4700 3500
	1    0    0    -1  
$EndComp
$Comp
L Device:C C3
U 1 1 5D88AE65
P 5570 3320
F 0 "C3" H 5685 3366 50  0000 L CNN
F 1 "C" H 5685 3275 50  0000 L CNN
F 2 "Capacitors_THT:CP_Radial_Tantal_D5.0mm_P2.50mm" H 5608 3170 50  0001 C CNN
F 3 "~" H 5570 3320 50  0001 C CNN
	1    5570 3320
	1    0    0    -1  
$EndComp
$Comp
L Device:C C1
U 1 1 5D88B17F
P 4550 3650
F 0 "C1" H 4665 3696 50  0000 L CNN
F 1 "22pF" H 4665 3605 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4588 3500 50  0001 C CNN
F 3 "~" H 4550 3650 50  0001 C CNN
	1    4550 3650
	1    0    0    -1  
$EndComp
$Comp
L Connector:AVR-ISP-6 J2
U 1 1 5D88F43E
P 3960 1740
F 0 "J2" V 3543 1790 50  0000 C CNN
F 1 "AVR-ISP-6" V 3634 1790 50  0000 C CNN
F 2 "pin_headers:PinHeader_2x03_P2.54mm_Vertical" V 3710 1790 50  0001 C CNN
F 3 " ~" H 2685 1190 50  0001 C CNN
	1    3960 1740
	0    1    1    0   
$EndComp
$Comp
L Connector:USB_B J10
U 1 1 5D88FC1B
P 6650 1970
F 0 "J10" V 6661 2300 50  0000 L CNN
F 1 "USB_B" V 6752 2300 50  0000 L CNN
F 2 "Connectors_USB:USB_B_OST_USB-B1HSxx_Horizontal" H 6800 1920 50  0001 C CNN
F 3 " ~" H 6800 1920 50  0001 C CNN
	1    6650 1970
	0    1    1    0   
$EndComp
$Comp
L Device:R R3
U 1 1 5D891896
P 6650 2420
F 0 "R3" H 6580 2374 50  0000 R CNN
F 1 "68R" V 6640 2500 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6580 2420 50  0001 C CNN
F 3 "~" H 6650 2420 50  0001 C CNN
	1    6650 2420
	-1   0    0    1   
$EndComp
$Comp
L Device:R R1
U 1 1 5D891DAC
P 6340 2420
F 0 "R1" H 6490 2370 50  0000 R CNN
F 1 "2.2k" V 6330 2490 50  0000 R CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6270 2420 50  0001 C CNN
F 3 "~" H 6340 2420 50  0001 C CNN
	1    6340 2420
	-1   0    0    1   
$EndComp
$Comp
L Device:D_Zener D1
U 1 1 5D8921AA
P 6550 2720
F 0 "D1" V 6504 2799 50  0000 L CNN
F 1 "D_Zener" V 6595 2799 50  0000 L CNN
F 2 "Diodes_THT:D_T-1_P12.70mm_Horizontal" H 6550 2720 50  0001 C CNN
F 3 "~" H 6550 2720 50  0001 C CNN
	1    6550 2720
	0    1    1    0   
$EndComp
$Comp
L Device:C C2
U 1 1 5D893355
P 4850 3650
F 0 "C2" H 4965 3696 50  0000 L CNN
F 1 "22pF" H 4965 3605 50  0000 L CNN
F 2 "Capacitors_THT:C_Disc_D3.0mm_W1.6mm_P2.50mm" H 4888 3500 50  0001 C CNN
F 3 "~" H 4850 3650 50  0001 C CNN
	1    4850 3650
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0101
U 1 1 5D89B2D4
P 4550 3800
F 0 "#PWR0101" H 4550 3550 50  0001 C CNN
F 1 "GND" H 4555 3627 50  0000 C CNN
F 2 "" H 4550 3800 50  0001 C CNN
F 3 "" H 4550 3800 50  0001 C CNN
	1    4550 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0102
U 1 1 5D89B92C
P 4850 3800
F 0 "#PWR0102" H 4850 3550 50  0001 C CNN
F 1 "GND" H 4855 3627 50  0000 C CNN
F 2 "" H 4850 3800 50  0001 C CNN
F 3 "" H 4850 3800 50  0001 C CNN
	1    4850 3800
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0103
U 1 1 5D89C97C
P 5570 3470
F 0 "#PWR0103" H 5570 3220 50  0001 C CNN
F 1 "GND" H 5575 3297 50  0000 C CNN
F 2 "" H 5570 3470 50  0001 C CNN
F 3 "" H 5570 3470 50  0001 C CNN
	1    5570 3470
	1    0    0    -1  
$EndComp
$Comp
L Device:R R2
U 1 1 5D8AE5C9
P 6550 2420
F 0 "R2" H 6390 2480 50  0000 L CNN
F 1 "68R" V 6550 2340 50  0000 L CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 6480 2420 50  0001 C CNN
F 3 "~" H 6550 2420 50  0001 C CNN
	1    6550 2420
	1    0    0    -1  
$EndComp
$Comp
L Device:D_Zener D2
U 1 1 5D8AF3DB
P 6650 2720
F 0 "D2" V 6604 2799 50  0000 L CNN
F 1 "D_Zener" V 6695 2799 50  0000 L CNN
F 2 "Diodes_THT:D_T-1_P12.70mm_Horizontal" H 6650 2720 50  0001 C CNN
F 3 "~" H 6650 2720 50  0001 C CNN
	1    6650 2720
	0    1    1    0   
$EndComp
Wire Wire Line
	6340 2570 6340 2580
Wire Wire Line
	6340 2570 6550 2570
Connection ~ 6340 2570
Connection ~ 6550 2570
$Comp
L power:VCC #PWR0104
U 1 1 5D8BFBCA
P 6850 2270
F 0 "#PWR0104" H 6850 2120 50  0001 C CNN
F 1 "VCC" V 6867 2398 50  0000 L CNN
F 2 "" H 6850 2270 50  0001 C CNN
F 3 "" H 6850 2270 50  0001 C CNN
	1    6850 2270
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0105
U 1 1 5D8C0268
P 6340 2270
F 0 "#PWR0105" H 6340 2120 50  0001 C CNN
F 1 "VCC" H 6357 2443 50  0000 C CNN
F 2 "" H 6340 2270 50  0001 C CNN
F 3 "" H 6340 2270 50  0001 C CNN
	1    6340 2270
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0106
U 1 1 5D8C09BB
P 4460 1640
F 0 "#PWR0106" H 4460 1490 50  0001 C CNN
F 1 "VCC" H 4477 1813 50  0000 C CNN
F 2 "" H 4460 1640 50  0001 C CNN
F 3 "" H 4460 1640 50  0001 C CNN
	1    4460 1640
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0107
U 1 1 5D8C20FB
P 3580 2560
F 0 "#PWR0107" H 3580 2410 50  0001 C CNN
F 1 "VCC" H 3597 2733 50  0000 C CNN
F 2 "" H 3580 2560 50  0001 C CNN
F 3 "" H 3580 2560 50  0001 C CNN
	1    3580 2560
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0108
U 1 1 5D8C3631
P 3680 2560
F 0 "#PWR0108" H 3680 2410 50  0001 C CNN
F 1 "VCC" H 3697 2733 50  0000 C CNN
F 2 "" H 3680 2560 50  0001 C CNN
F 3 "" H 3680 2560 50  0001 C CNN
	1    3680 2560
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0109
U 1 1 5D8C4DB9
P 3580 5560
F 0 "#PWR0109" H 3580 5310 50  0001 C CNN
F 1 "GND" H 3585 5387 50  0000 C CNN
F 2 "" H 3580 5560 50  0001 C CNN
F 3 "" H 3580 5560 50  0001 C CNN
	1    3580 5560
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0110
U 1 1 5D8C5C63
P 3560 1640
F 0 "#PWR0110" H 3560 1390 50  0001 C CNN
F 1 "GND" H 3565 1467 50  0000 C CNN
F 2 "" H 3560 1640 50  0001 C CNN
F 3 "" H 3560 1640 50  0001 C CNN
	1    3560 1640
	1    0    0    -1  
$EndComp
Text GLabel 6340 2570 0    50   BiDi ~ 0
D-
Text GLabel 6650 2570 2    50   BiDi ~ 0
D+
$Comp
L power:GND #PWR0111
U 1 1 5D8D062E
P 6250 1970
F 0 "#PWR0111" H 6250 1720 50  0001 C CNN
F 1 "GND" H 6255 1797 50  0000 C CNN
F 2 "" H 6250 1970 50  0001 C CNN
F 3 "" H 6250 1970 50  0001 C CNN
	1    6250 1970
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0112
U 1 1 5D8D10B9
P 6550 2870
F 0 "#PWR0112" H 6550 2620 50  0001 C CNN
F 1 "GND" H 6555 2697 50  0000 C CNN
F 2 "" H 6550 2870 50  0001 C CNN
F 3 "" H 6550 2870 50  0001 C CNN
	1    6550 2870
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0113
U 1 1 5D8D29C0
P 6650 2870
F 0 "#PWR0113" H 6650 2620 50  0001 C CNN
F 1 "GND" H 6655 2697 50  0000 C CNN
F 2 "" H 6650 2870 50  0001 C CNN
F 3 "" H 6650 2870 50  0001 C CNN
	1    6650 2870
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0114
U 1 1 5D8D409E
P 5570 3170
F 0 "#PWR0114" H 5570 3020 50  0001 C CNN
F 1 "VCC" H 5587 3343 50  0000 C CNN
F 2 "" H 5570 3170 50  0001 C CNN
F 3 "" H 5570 3170 50  0001 C CNN
	1    5570 3170
	1    0    0    -1  
$EndComp
Wire Wire Line
	4180 3460 4850 3460
Wire Wire Line
	4850 3460 4850 3500
Connection ~ 4850 3500
Wire Wire Line
	4180 3560 4550 3560
Wire Wire Line
	4550 3560 4550 3500
Connection ~ 4550 3500
Text GLabel 4180 4760 2    50   BiDi ~ 0
>D-
Text GLabel 4180 4960 2    50   BiDi ~ 0
>D+
Text GLabel 3860 2140 3    50   BiDi ~ 0
RST
Text GLabel 4180 4360 2    50   BiDi ~ 0
RST
Text GLabel 3960 2140 3    50   BiDi ~ 0
SCK
Text GLabel 4060 2140 3    50   BiDi ~ 0
MOSI
Text GLabel 4160 2140 3    50   BiDi ~ 0
MISO
Text GLabel 4180 3360 2    50   BiDi ~ 0
SCK
Text GLabel 4180 3260 2    50   BiDi ~ 0
MISO
Text GLabel 4180 3160 2    50   BiDi ~ 0
MOSI
Text GLabel 4180 4860 2    50   BiDi ~ 0
int1
$Comp
L Connector:Conn_01x02_Male J8
U 1 1 5D8F02D2
P 6080 4430
F 0 "J8" V 6142 4474 50  0000 L CNN
F 1 "jack" V 6233 4474 50  0000 L CNN
F 2 "pin_headers:PinHeader_1x02_P2.54mm_Vertical" H 6080 4430 50  0001 C CNN
F 3 "~" H 6080 4430 50  0001 C CNN
	1    6080 4430
	0    1    1    0   
$EndComp
Text GLabel 5980 4630 3    50   BiDi ~ 0
int1
$Comp
L power:GND #PWR0115
U 1 1 5D8F104B
P 6080 4630
F 0 "#PWR0115" H 6080 4380 50  0001 C CNN
F 1 "GND" H 6085 4457 50  0000 C CNN
F 2 "" H 6080 4630 50  0001 C CNN
F 3 "" H 6080 4630 50  0001 C CNN
	1    6080 4630
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x03_Male J3
U 1 1 5D8F2EED
P 4380 2960
F 0 "J3" H 4352 2892 50  0000 R CNN
F 1 "Conn_01x03_Male" H 4352 2983 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x03_P2.54mm_Vertical" H 4380 2960 50  0001 C CNN
F 3 "~" H 4380 2960 50  0001 C CNN
	1    4380 2960
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J7
U 1 1 5D8F6504
P 4380 5160
F 0 "J7" H 4352 5092 50  0000 R CNN
F 1 "Conn_01x03_Male" H 4352 5183 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x03_P2.54mm_Vertical" H 4380 5160 50  0001 C CNN
F 3 "~" H 4380 5160 50  0001 C CNN
	1    4380 5160
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x02_Male J5
U 1 1 5D8F73DB
P 4380 4660
F 0 "J5" H 4352 4542 50  0000 R CNN
F 1 "Conn_01x02_Male" H 4352 4633 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x02_P2.54mm_Vertical" H 4380 4660 50  0001 C CNN
F 3 "~" H 4380 4660 50  0001 C CNN
	1    4380 4660
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x03_Male J6
U 1 1 5D8F8451
P 4380 4860
F 0 "J6" H 4352 4792 50  0000 R CNN
F 1 "Conn_01x03_Male" H 4352 4883 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x03_P2.54mm_Vertical" H 4380 4860 50  0001 C CNN
F 3 "~" H 4380 4860 50  0001 C CNN
	1    4380 4860
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x06_Male J4
U 1 1 5D8F94C7
P 4380 4060
F 0 "J4" H 4352 3942 50  0000 R CNN
F 1 "Conn_01x06_Male" H 4352 4033 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x06_P2.54mm_Vertical" H 4380 4060 50  0001 C CNN
F 3 "~" H 4380 4060 50  0001 C CNN
	1    4380 4060
	-1   0    0    1   
$EndComp
$Comp
L Connector:Conn_01x06_Male J9
U 1 1 5D8FA932
P 6200 3820
F 0 "J9" V 6262 4064 50  0000 L CNN
F 1 "Conn_01x06_Male" V 6353 4064 50  0000 L CNN
F 2 "pin_headers:PinHeader_1x06_P2.54mm_Vertical" H 6200 3820 50  0001 C CNN
F 3 "~" H 6200 3820 50  0001 C CNN
	1    6200 3820
	0    1    1    0   
$EndComp
$Comp
L power:GND #PWR0116
U 1 1 5D8FB608
P 5900 4020
F 0 "#PWR0116" H 5900 3770 50  0001 C CNN
F 1 "GND" H 5905 3847 50  0000 C CNN
F 2 "" H 5900 4020 50  0001 C CNN
F 3 "" H 5900 4020 50  0001 C CNN
	1    5900 4020
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0117
U 1 1 5D8FCE21
P 6000 4020
F 0 "#PWR0117" H 6000 3770 50  0001 C CNN
F 1 "GND" H 6005 3847 50  0000 C CNN
F 2 "" H 6000 4020 50  0001 C CNN
F 3 "" H 6000 4020 50  0001 C CNN
	1    6000 4020
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0118
U 1 1 5D8FD213
P 6100 4020
F 0 "#PWR0118" H 6100 3770 50  0001 C CNN
F 1 "GND" H 6105 3847 50  0000 C CNN
F 2 "" H 6100 4020 50  0001 C CNN
F 3 "" H 6100 4020 50  0001 C CNN
	1    6100 4020
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0119
U 1 1 5D8FD517
P 6200 4020
F 0 "#PWR0119" H 6200 3770 50  0001 C CNN
F 1 "GND" H 6205 3847 50  0000 C CNN
F 2 "" H 6200 4020 50  0001 C CNN
F 3 "" H 6200 4020 50  0001 C CNN
	1    6200 4020
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0120
U 1 1 5D8FD82A
P 6300 4020
F 0 "#PWR0120" H 6300 3770 50  0001 C CNN
F 1 "GND" H 6305 3847 50  0000 C CNN
F 2 "" H 6300 4020 50  0001 C CNN
F 3 "" H 6300 4020 50  0001 C CNN
	1    6300 4020
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0121
U 1 1 5D8FDC12
P 6400 4020
F 0 "#PWR0121" H 6400 3770 50  0001 C CNN
F 1 "GND" H 6405 3847 50  0000 C CNN
F 2 "" H 6400 4020 50  0001 C CNN
F 3 "" H 6400 4020 50  0001 C CNN
	1    6400 4020
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J1
U 1 1 5D8FFD29
P 2780 2860
F 0 "J1" H 2888 3041 50  0000 C CNN
F 1 "Conn_01x02_Male" H 2888 2950 50  0000 C CNN
F 2 "pin_headers:PinHeader_1x02_P2.54mm_Vertical" H 2780 2860 50  0001 C CNN
F 3 "~" H 2780 2860 50  0001 C CNN
	1    2780 2860
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR0122
U 1 1 5D900E05
P 2980 2960
F 0 "#PWR0122" H 2980 2710 50  0001 C CNN
F 1 "GND" H 2985 2787 50  0000 C CNN
F 2 "" H 2980 2960 50  0001 C CNN
F 3 "" H 2980 2960 50  0001 C CNN
	1    2980 2960
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x04_Male J11
U 1 1 5D89D8E1
P 7400 2690
F 0 "J11" H 7372 2572 50  0000 R CNN
F 1 "Conn_01x04_Male" H 7372 2663 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x04_P2.54mm_Vertical" H 7400 2690 50  0001 C CNN
F 3 "~" H 7400 2690 50  0001 C CNN
	1    7400 2690
	1    0    0    1   
$EndComp
Text GLabel 7600 2690 2    50   BiDi ~ 0
D+
Text GLabel 7600 2590 2    50   BiDi ~ 0
D-
$Comp
L power:GND #PWR02
U 1 1 5D89FFC5
P 7600 2790
F 0 "#PWR02" H 7600 2540 50  0001 C CNN
F 1 "GND" H 7605 2617 50  0000 C CNN
F 2 "" H 7600 2790 50  0001 C CNN
F 3 "" H 7600 2790 50  0001 C CNN
	1    7600 2790
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR01
U 1 1 5D8A0D37
P 7600 2490
F 0 "#PWR01" H 7600 2340 50  0001 C CNN
F 1 "VCC" V 7617 2618 50  0000 L CNN
F 2 "" H 7600 2490 50  0001 C CNN
F 3 "" H 7600 2490 50  0001 C CNN
	1    7600 2490
	0    1    1    0   
$EndComp
$Comp
L Connector:Conn_01x04_Male J12
U 1 1 5D8B0670
P 7410 3500
F 0 "J12" H 7382 3382 50  0000 R CNN
F 1 "Conn_01x04_Male" H 7382 3473 50  0000 R CNN
F 2 "pin_headers:PinHeader_1x04_P2.54mm_Vertical" H 7410 3500 50  0001 C CNN
F 3 "~" H 7410 3500 50  0001 C CNN
	1    7410 3500
	1    0    0    1   
$EndComp
Text GLabel 7610 3500 2    50   BiDi ~ 0
>D+
Text GLabel 7610 3400 2    50   BiDi ~ 0
>D-
$Comp
L power:GND #PWR0123
U 1 1 5D8B067C
P 7610 3600
F 0 "#PWR0123" H 7610 3350 50  0001 C CNN
F 1 "GND" H 7615 3427 50  0000 C CNN
F 2 "" H 7610 3600 50  0001 C CNN
F 3 "" H 7610 3600 50  0001 C CNN
	1    7610 3600
	1    0    0    -1  
$EndComp
$Comp
L power:VCC #PWR0124
U 1 1 5D8B0686
P 7610 3300
F 0 "#PWR0124" H 7610 3150 50  0001 C CNN
F 1 "VCC" V 7627 3428 50  0000 L CNN
F 2 "" H 7610 3300 50  0001 C CNN
F 3 "" H 7610 3300 50  0001 C CNN
	1    7610 3300
	0    1    1    0   
$EndComp
$Comp
L Device:R R4
U 1 1 5D8D5C27
P 3710 2140
F 0 "R4" V 3503 2140 50  0000 C CNN
F 1 "10k" V 3710 2140 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 3640 2140 50  0001 C CNN
F 3 "~" H 3710 2140 50  0001 C CNN
	1    3710 2140
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0125
U 1 1 5D8DD6C1
P 3560 2140
F 0 "#PWR0125" H 3560 1990 50  0001 C CNN
F 1 "VCC" H 3577 2313 50  0000 C CNN
F 2 "" H 3560 2140 50  0001 C CNN
F 3 "" H 3560 2140 50  0001 C CNN
	1    3560 2140
	1    0    0    -1  
$EndComp
$Comp
L Connector:Conn_01x02_Male J13
U 1 1 5D8E0BA7
P 5330 1960
F 0 "J13" H 5438 2141 50  0000 C CNN
F 1 "Conn_01x02_Male" H 5438 2050 50  0000 C CNN
F 2 "pin_headers:PinHeader_1x02_P2.54mm_Vertical" H 5330 1960 50  0001 C CNN
F 3 "~" H 5330 1960 50  0001 C CNN
	1    5330 1960
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0126
U 1 1 5D8E1360
P 5230 2160
F 0 "#PWR0126" H 5230 2010 50  0001 C CNN
F 1 "VCC" H 5247 2333 50  0000 C CNN
F 2 "" H 5230 2160 50  0001 C CNN
F 3 "" H 5230 2160 50  0001 C CNN
	1    5230 2160
	-1   0    0    1   
$EndComp
$Comp
L power:GND #PWR0127
U 1 1 5D8E301E
P 5330 2160
F 0 "#PWR0127" H 5330 1910 50  0001 C CNN
F 1 "GND" H 5335 1987 50  0000 C CNN
F 2 "" H 5330 2160 50  0001 C CNN
F 3 "" H 5330 2160 50  0001 C CNN
	1    5330 2160
	1    0    0    -1  
$EndComp
$Comp
L Device:LED D3
U 1 1 5D8EF9E6
P 5320 2640
F 0 "D3" H 5313 2856 50  0000 C CNN
F 1 "LED" H 5313 2765 50  0000 C CNN
F 2 "LEDs:LED_D3.0mm" H 5320 2640 50  0001 C CNN
F 3 "~" H 5320 2640 50  0001 C CNN
	1    5320 2640
	1    0    0    -1  
$EndComp
$Comp
L Device:R R5
U 1 1 5D8F161A
P 5620 2640
F 0 "R5" V 5413 2640 50  0000 C CNN
F 1 "220R" V 5620 2640 50  0000 C CNN
F 2 "Resistors_THT:R_Axial_DIN0309_L9.0mm_D3.2mm_P12.70mm_Horizontal" V 5550 2640 50  0001 C CNN
F 3 "~" H 5620 2640 50  0001 C CNN
	1    5620 2640
	0    1    1    0   
$EndComp
$Comp
L power:VCC #PWR0128
U 1 1 5D8FB7A3
P 5770 2640
F 0 "#PWR0128" H 5770 2490 50  0001 C CNN
F 1 "VCC" H 5787 2813 50  0000 C CNN
F 2 "" H 5770 2640 50  0001 C CNN
F 3 "" H 5770 2640 50  0001 C CNN
	1    5770 2640
	1    0    0    -1  
$EndComp
$Comp
L power:GND #PWR?
U 1 1 5D908894
P 5170 2640
F 0 "#PWR?" H 5170 2390 50  0001 C CNN
F 1 "GND" H 5175 2467 50  0000 C CNN
F 2 "" H 5170 2640 50  0001 C CNN
F 3 "" H 5170 2640 50  0001 C CNN
	1    5170 2640
	1    0    0    -1  
$EndComp
$EndSCHEMATC
